package com.am.loanwiper;

import com.am.loanwiper.domain.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EmiSimulationTest {

    private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_EVEN;
    private static final int SCALE = 8;
    private static List<BigDecimal> expectedMonthlyInterestPayment;
    private static List<BigDecimal> expectedMonthlyCapitalPayment;
    private static List<BigDecimal> expectedMonthlyPayment;

    @BeforeEach
    public void before() {
        expectedMonthlyInterestPayment = new ArrayList<>();
        expectedMonthlyCapitalPayment = new ArrayList<>();
        expectedMonthlyPayment = new ArrayList<>();
    }

    @Test
    public void basicEmiSimulationTest() {
        //given
        expectedMonthlyInterestPayment.add(new BigDecimal("50"));
        expectedMonthlyInterestPayment.add(new BigDecimal("46.112083"));
        expectedMonthlyInterestPayment.add(new BigDecimal("42.175568"));
        expectedMonthlyInterestPayment.add(new BigDecimal("38.189847"));
        expectedMonthlyInterestPayment.add(new BigDecimal("34.154305"));
        expectedMonthlyInterestPayment.add(new BigDecimal("30.068317"));
        expectedMonthlyInterestPayment.add(new BigDecimal("25.931255"));
        expectedMonthlyInterestPayment.add(new BigDecimal("21.742480"));
        expectedMonthlyInterestPayment.add(new BigDecimal("17.501345"));
        expectedMonthlyInterestPayment.add(new BigDecimal("13.207197"));
        expectedMonthlyInterestPayment.add(new BigDecimal("8.8593700"));
        expectedMonthlyInterestPayment.add(new BigDecimal("4.4571963"));

        expectedMonthlyCapitalPayment.add(new BigDecimal("311.03328"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("314.92120"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("318.85771"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("322.84343"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("326.87898"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("330.96496"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("335.10202"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("339.29080"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("343.53194"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("347.82608"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("352.17391"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("356.57608"));

        for (int i = 0; i < 12; i++) {
            expectedMonthlyPayment.add(new BigDecimal("361.03328"));
        }

        EmiSimulation simulation = new EmiSimulation();
        Amount capital = new Amount(4000);
        Amount interest = new Amount(0.15);
        int monthsLeft = 12;

        //when
        IPaymentPlan paymentPlan = simulation.simulate(capital, interest, monthsLeft);

        //then
        validateResult(paymentPlan, "4332.39936", "332.3989633", 12);
    }

    @Test
    public void overpaymentsEmiSimulationTest() {
        //given
        expectedMonthlyInterestPayment.add(new BigDecimal("12.5"));
        expectedMonthlyInterestPayment.add(new BigDecimal("10.061725"));
        expectedMonthlyInterestPayment.add(new BigDecimal("6.3429723"));
        expectedMonthlyInterestPayment.add(new BigDecimal("4.2548582"));
        expectedMonthlyInterestPayment.add(new BigDecimal("2.1406427"));

        expectedMonthlyCapitalPayment.add(new BigDecimal("195.06197"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("297.50024"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("167.04913"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("169.13724"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("171.25146"));

        expectedMonthlyPayment.add(new BigDecimal("207.56197"));
        expectedMonthlyPayment.add(new BigDecimal("307.56197"));
        expectedMonthlyPayment.add(new BigDecimal("173.39210"));
        expectedMonthlyPayment.add(new BigDecimal("173.39210"));
        expectedMonthlyPayment.add(new BigDecimal("173.39210"));

        EmiSimulation simulation = new EmiSimulation();
        Amount capital = new Amount(1000);
        Amount interest = new Amount(0.15);
        int monthsLeft = 5;

        IOverpayPlan overpayments = new OverpayPlan();
        overpayments.addExtraPayment(2, new BigDecimal("100"));

        //when
        IPaymentPlan paymentPlan = simulation.simulate(capital, interest, monthsLeft, overpayments);

        //then
        validateResult(paymentPlan, "1035.30024000", "35.30019820", 5);
    }

    @Test
    public void overpaymentsEarlierPayOffEmiSimulationTest() {
        //given
        expectedMonthlyInterestPayment.add(new BigDecimal("12.5"));
        expectedMonthlyInterestPayment.add(new BigDecimal("10.061725"));
        expectedMonthlyInterestPayment.add(new BigDecimal("7.5929723"));
        expectedMonthlyInterestPayment.add(new BigDecimal("5.0933598"));

        expectedMonthlyCapitalPayment.add(new BigDecimal("195.06197"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("197.50024"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("199.96900"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("407.46879"));

        expectedMonthlyPayment.add(new BigDecimal("207.56197"));
        expectedMonthlyPayment.add(new BigDecimal("207.56197"));
        expectedMonthlyPayment.add(new BigDecimal("207.56197"));
        expectedMonthlyPayment.add(new BigDecimal("412.56215"));

        EmiSimulation simulation = new EmiSimulation();
        Amount capital = new Amount(1000);
        Amount interest = new Amount(0.15);
        int monthsLeft = 5;

        IOverpayPlan overpayments = new OverpayPlan();
        overpayments.addExtraPayment(4, new BigDecimal("500"));

        //when
        IPaymentPlan paymentPlan = simulation.simulate(capital, interest, monthsLeft, overpayments);

        //then
        validateResult(paymentPlan, "1035.24806", "35.2480571", 4);
    }

    private void validateResult(IPaymentPlan paymentPlan, String expectedTotalPayment, String expectedInterestPayment, int expectedInstallments) {
        assertEquals(expectedInstallments, paymentPlan.getPaymentSchedule().size());
        for (int i = 1; i <= paymentPlan.getPaymentSchedule().size(); i++) {
            MonthlyInstallment monthlyInstallment = paymentPlan.getPaymentSchedule().get(i);
            assertEquals(i, monthlyInstallment.getMonthNumber());
            assertEquals(expectedMonthlyInterestPayment.get(i - 1).setScale(SCALE, ROUNDING_MODE), monthlyInstallment.getMonthlyInterestPayment().setScale(SCALE, ROUNDING_MODE));
            assertEquals(expectedMonthlyCapitalPayment.get(i - 1).setScale(SCALE, ROUNDING_MODE), monthlyInstallment.getMonthlyCapitalPayment().setScale(SCALE, ROUNDING_MODE));
            assertEquals(expectedMonthlyPayment.get(i - 1).setScale(SCALE, ROUNDING_MODE), monthlyInstallment.getMonthlyPayment().setScale(SCALE, ROUNDING_MODE));
        }
        assertEquals(0, new BigDecimal(expectedInterestPayment).setScale(SCALE, ROUNDING_MODE).compareTo(paymentPlan.getTotalInterestPaid().setScale(SCALE, ROUNDING_MODE)));
        assertEquals(0, new BigDecimal(expectedTotalPayment).setScale(SCALE, ROUNDING_MODE).compareTo(paymentPlan.getTotalPaid().setScale(SCALE, ROUNDING_MODE)));
    }
}
