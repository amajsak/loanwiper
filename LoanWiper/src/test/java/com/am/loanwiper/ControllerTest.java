package com.am.loanwiper;

import com.am.loanwiper.controller.Controller;
import com.am.loanwiper.domain.*;
import com.am.loanwiper.service.IRatesService;
import com.am.loanwiper.service.ISimulationService;
import org.hamcrest.core.StringStartsWith;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(Controller.class)
public class ControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IRatesService ratesService;

    @MockBean
    private ISimulationService simulationService;

    @Test
    public void testRatesOk() throws Exception {
        given(this.ratesService.getExchangeRate(Currency.EUR, Currency.USD)).willReturn(1.2);

        this.mvc.perform(get("/rates?from=eur&to=usd").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().string("{\"rate\":1.2}"));
    }

    @Test
    public void testRatesMissingParam() throws Exception {
        this.mvc.perform(get("/rates?from=eur").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(content().string("{\"code\":\"400\",\"message\":\"Required Currency parameter 'to' is not present\"}"));
    }

    @Test
    public void testRatesInvalidCurrency() throws Exception {
        this.mvc.perform(get("/rates?from=eur&to=abc").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(content().string("{\"code\":\"400\",\"message\":\"Unknown currency abc, Allowed values are [PLN, EUR, USD, CHF, GBP]\"}"));
    }

    @Test
    public void testCurrencies() throws Exception {
        this.mvc.perform(get("/currencies").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().string("[{\"key\":\"pln\",\"symbol\":\"PLN\"},{\"key\":\"eur\",\"symbol\":\"€\"},{\"key\":\"usd\",\"symbol\":\"$\"},{\"key\":\"chf\",\"symbol\":\"CHF\"},{\"key\":\"gbp\",\"symbol\":\"£\"}]"));
    }

    @Test
    public void testGetPlan() throws Exception {
        IPaymentPlan plan = new BasicPaymentPlan();
        MonthlyInstallment mi = new MonthlyInstallment(BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ONE);
        mi.setMonthNumber(1);
        plan.addPayment(mi);
        given(this.simulationService.simulate(any(), any(), any())).willReturn(plan);

        this.mvc.perform(get("/plan?capital=1000&interest=0.01&months=10&type=vmi").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().string(new StringStartsWith("{\"paymentSchedule\":{\"1\":{")));
    }

    @Test
    public void testPostPlan() throws Exception {
        IPaymentPlan plan = new BasicPaymentPlan();
        MonthlyInstallment mi = new MonthlyInstallment(BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ONE);
        mi.setMonthNumber(1);
        plan.addPayment(mi);
        given(this.simulationService.simulate(any(), any(), any())).willReturn(plan);

        Object[] postParams = new Object[1];
        this.mvc.perform(post("/plan?capital=1000&interest=0.01&months=10&type=vmi", postParams).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().string(new StringStartsWith("{\"paymentSchedule\":{\"1\":{")));
    }

    @Test
    public void testGetPlanMissingParam() throws Exception {
        this.mvc.perform(get("/plan?capital=1000&interest=0.01").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(content().string("{\"code\":\"400\",\"message\":\"plan.arg0.months: must be greater than or equal to 1\"}"));
    }

    @Test
    public void testGetPlanArgInvalidType() throws Exception {
        this.mvc.perform(get("/plan?capital=1000&interest=0.01&months=abc").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(content().string("{\"code\":\"400\",\"message\":\"plan.arg0.months: must be greater than or equal to 1\"}"));
    }

    @Test
    public void testGetPlanConstraintViolation() throws Exception {
        this.mvc.perform(get("/plan?capital=1000&interest=0.01&months=601").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andExpect(content().string("{\"code\":\"400\",\"message\":\"plan.arg0.months: must be less than or equal to 600\"}"));
    }
}
