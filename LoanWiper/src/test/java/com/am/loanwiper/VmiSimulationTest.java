package com.am.loanwiper;

import com.am.loanwiper.domain.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VmiSimulationTest {

    private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_EVEN;
    private static final int SCALE = 8;
    private static List<BigDecimal> expectedMonthlyInterestPayment;
    private static List<BigDecimal> expectedMonthlyCapitalPayment;
    private static List<BigDecimal> expectedMonthlyPayment;

    @BeforeEach
    public void before() {
        expectedMonthlyInterestPayment = new ArrayList<>();
        expectedMonthlyCapitalPayment = new ArrayList<>();
        expectedMonthlyPayment = new ArrayList<>();
    }

    @Test
    public void basicSimulationTest() {
        //given
        expectedMonthlyInterestPayment.add(new BigDecimal("50"));
        expectedMonthlyInterestPayment.add(new BigDecimal("45.83333"));
        expectedMonthlyInterestPayment.add(new BigDecimal("41.66667"));
        expectedMonthlyInterestPayment.add(new BigDecimal("37.5"));
        expectedMonthlyInterestPayment.add(new BigDecimal("33.33333"));
        expectedMonthlyInterestPayment.add(new BigDecimal("29.16667"));
        expectedMonthlyInterestPayment.add(new BigDecimal("25"));
        expectedMonthlyInterestPayment.add(new BigDecimal("20.83333"));
        expectedMonthlyInterestPayment.add(new BigDecimal("16.66667"));
        expectedMonthlyInterestPayment.add(new BigDecimal("12.5"));
        expectedMonthlyInterestPayment.add(new BigDecimal("8.33333"));
        expectedMonthlyInterestPayment.add(new BigDecimal("4.16667"));

        expectedMonthlyPayment.add(new BigDecimal("383.33333"));
        expectedMonthlyPayment.add(new BigDecimal("379.16667"));
        expectedMonthlyPayment.add(new BigDecimal("375"));
        expectedMonthlyPayment.add(new BigDecimal("370.83333"));
        expectedMonthlyPayment.add(new BigDecimal("366.66667"));
        expectedMonthlyPayment.add(new BigDecimal("362.5"));
        expectedMonthlyPayment.add(new BigDecimal("358.33333"));
        expectedMonthlyPayment.add(new BigDecimal("354.16667"));
        expectedMonthlyPayment.add(new BigDecimal("350"));
        expectedMonthlyPayment.add(new BigDecimal("345.83333"));
        expectedMonthlyPayment.add(new BigDecimal("341.66668"));
        expectedMonthlyPayment.add(new BigDecimal("337.49997"));

        expectedMonthlyCapitalPayment.add(new BigDecimal("333.33333"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("333.33334"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("333.33333"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("333.33333"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("333.33334"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("333.33333"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("333.33333"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("333.33334"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("333.33333"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("333.33333"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("333.33335"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("333.3333"));

        VmiSimulation simulation = new VmiSimulation();
        Amount capital = new Amount(4000);
        Amount interest = new Amount(0.15);
        int monthsLeft = 12;

        //when
        IPaymentPlan paymentPlan = simulation.simulate(capital, interest, monthsLeft);

        //then
        validateResult(paymentPlan, "4324.99998", "325", 12);
    }

    @Test
    public void overpaymentsVmiSimulationTest() {
        //given
        expectedMonthlyInterestPayment.add(new BigDecimal("12.5"));
        expectedMonthlyInterestPayment.add(new BigDecimal("10"));
        expectedMonthlyInterestPayment.add(new BigDecimal("6.25"));
        expectedMonthlyInterestPayment.add(new BigDecimal("4.1666700"));
        expectedMonthlyInterestPayment.add(new BigDecimal("2.0833300"));

        expectedMonthlyCapitalPayment.add(new BigDecimal("200"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("300"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("166.66667"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("166.66666"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("166.66670"));

        expectedMonthlyPayment.add(new BigDecimal("212.5"));
        expectedMonthlyPayment.add(new BigDecimal("310"));
        expectedMonthlyPayment.add(new BigDecimal("172.91667"));
        expectedMonthlyPayment.add(new BigDecimal("170.83333"));
        expectedMonthlyPayment.add(new BigDecimal("168.75003"));

        VmiSimulation simulation = new VmiSimulation();
        Amount capital = new Amount(1000);
        Amount interest = new Amount(0.15);
        int monthsLeft = 5;

        IOverpayPlan overpayments = new OverpayPlan();
        overpayments.addExtraPayment(2, new BigDecimal("100"));

        //when
        IPaymentPlan paymentPlan = simulation.simulate(capital, interest, monthsLeft, overpayments);

        //then
        validateResult(paymentPlan, "1035.00003000", "35", 5);
    }

    @Test
    public void overpaymentsEarlierPayOffVmiSimulationTest() {
        //given
        expectedMonthlyInterestPayment.add(new BigDecimal("12.5"));
        expectedMonthlyInterestPayment.add(new BigDecimal("10"));
        expectedMonthlyInterestPayment.add(new BigDecimal("7.5"));
        expectedMonthlyInterestPayment.add(new BigDecimal("5"));

        expectedMonthlyCapitalPayment.add(new BigDecimal("200"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("200"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("200"));
        expectedMonthlyCapitalPayment.add(new BigDecimal("400"));

        expectedMonthlyPayment.add(new BigDecimal("212.5"));
        expectedMonthlyPayment.add(new BigDecimal("210"));
        expectedMonthlyPayment.add(new BigDecimal("207.5"));
        expectedMonthlyPayment.add(new BigDecimal("405"));

        VmiSimulation simulation = new VmiSimulation();
        Amount capital = new Amount(1000);
        Amount interest = new Amount(0.15);
        int monthsLeft = 5;

        IOverpayPlan overpayments = new OverpayPlan();
        overpayments.addExtraPayment(4, new BigDecimal("500"));

        //when
        IPaymentPlan paymentPlan = simulation.simulate(capital, interest, monthsLeft, overpayments);

        //then
        validateResult(paymentPlan, "1035", "35", 4);
    }

    private void validateResult(IPaymentPlan paymentPlan, String expectedTotalPayment, String expectedInterestPayment, int expectedInstallments) {
        assertEquals(expectedInstallments, paymentPlan.getPaymentSchedule().size());
        for (int i = 1; i <= paymentPlan.getPaymentSchedule().size(); i++) {
            MonthlyInstallment monthlyInstallment = paymentPlan.getPaymentSchedule().get(i);
            assertEquals(i, monthlyInstallment.getMonthNumber());
            assertEquals(expectedMonthlyInterestPayment.get(i - 1).setScale(SCALE, ROUNDING_MODE), monthlyInstallment.getMonthlyInterestPayment().setScale(SCALE, ROUNDING_MODE));
            assertEquals(expectedMonthlyCapitalPayment.get(i - 1).setScale(SCALE, ROUNDING_MODE), monthlyInstallment.getMonthlyCapitalPayment().setScale(SCALE, ROUNDING_MODE));
            assertEquals(expectedMonthlyPayment.get(i - 1).setScale(SCALE, ROUNDING_MODE), monthlyInstallment.getMonthlyPayment().setScale(SCALE, ROUNDING_MODE));
        }
        assertEquals(0, new BigDecimal(expectedInterestPayment).setScale(SCALE, ROUNDING_MODE).compareTo(paymentPlan.getTotalInterestPaid().setScale(SCALE, ROUNDING_MODE)));
        assertEquals(0, new BigDecimal(expectedTotalPayment).setScale(SCALE, ROUNDING_MODE).compareTo(paymentPlan.getTotalPaid().setScale(SCALE, ROUNDING_MODE)));
    }
}
