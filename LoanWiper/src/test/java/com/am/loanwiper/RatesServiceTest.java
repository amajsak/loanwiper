package com.am.loanwiper;

import com.am.loanwiper.domain.Currency;
import com.am.loanwiper.model.Rate;
import com.am.loanwiper.model.RatesTable;
import com.am.loanwiper.service.IRatesService;
import com.am.loanwiper.service.RatesService;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RatesServiceTest {

    private final static String RATES_API_URL = "http://api.nbp.pl/api/exchangerates/rates/a/{currency}";

    private static RestTemplate restTemplate;
    private static IRatesService ratesService = new RatesService();

    @BeforeAll
    public static void setUp() {
        restTemplate = mock(RestTemplate.class);
        ratesService.setRestTemplate(restTemplate);

        RatesTable ratesTableEur = new RatesTable();
        Rate rateEur = new Rate();
        rateEur.setEffectiveDate("date");
        rateEur.setMid("4.2");
        ratesTableEur.setCode("eur");
        ratesTableEur.setRates(Arrays.asList(rateEur));
        RatesTable ratesTableUsd = new RatesTable();
        Rate rateUsd = new Rate();
        rateUsd.setEffectiveDate("date");
        rateUsd.setMid("3.9");
        ratesTableUsd.setCode("usd");
        ratesTableUsd.setRates(Arrays.asList(rateUsd));

        Map<String, String> urlVariablesUsd = new HashMap<>();
        urlVariablesUsd.put("currency", "usd");
        Map<String, String> urlVariablesEur = new HashMap<>();
        urlVariablesEur.put("currency", "eur");

        when(restTemplate.getForObject(eq(RATES_API_URL), anyObject(), argThat(new MapMatcher("EUR")))).thenReturn(ratesTableEur);
        when(restTemplate.getForObject(eq(RATES_API_URL), anyObject(), argThat(new MapMatcher("USD")))).thenReturn(ratesTableUsd);
    }

    @Test
    public void testNonPLN() {

        //when
        double exchangeRate = ratesService.getExchangeRate(Currency.EUR, Currency.USD);

        //then
        assertEquals(1.076923076923077, exchangeRate);
    }

    @Test
    public void testFromPLN() {

        //when
        double exchangeRate = ratesService.getExchangeRate(Currency.PLN, Currency.USD);

        //then
        assertEquals(0.25641025641025644, exchangeRate);
    }

    @Test
    public void testToPLN() {

        //when
        double exchangeRate = ratesService.getExchangeRate(Currency.EUR, Currency.PLN);

        //then
        assertEquals(4.2, exchangeRate);
    }

    private static class MapMatcher extends BaseMatcher<HashMap<String, String>> {
        private String currency;

        public MapMatcher(String currency) {
            this.currency = currency;
        }

        public boolean matches(Object otherBar) {
            return otherBar instanceof HashMap && ((HashMap<String, String>) otherBar).get("currency").equals(currency);
        }

        @Override
        public void describeTo(Description description) {
        }
    }
}
