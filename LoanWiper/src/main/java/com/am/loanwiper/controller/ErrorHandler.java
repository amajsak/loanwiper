package com.am.loanwiper.controller;

import com.am.loanwiper.model.RestError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler
    public ResponseEntity<RestError> missingParameterExceptionHandler(MissingServletRequestParameterException ex) {
        RestError error = new RestError();
        error.setCode(HttpStatus.BAD_REQUEST.toString());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<RestError> argTypeMismatchExceptionHandler(MethodArgumentTypeMismatchException ex) {
        RestError error = new RestError();
        error.setCode(HttpStatus.BAD_REQUEST.toString());
        error.setMessage(ex.getCause().getMessage());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<RestError> constraintViolationExceptionHandler(ConstraintViolationException ex) {
        RestError error = new RestError();
        error.setCode(HttpStatus.BAD_REQUEST.toString());
        StringBuilder sb = new StringBuilder();
        for (ConstraintViolation e : ex.getConstraintViolations()) {
            sb.append(e.getPropertyPath()).append(": " + e.getMessage());
        }
        error.setMessage(sb.toString());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<RestError> msgNotReadableExceptionHandler(HttpMessageNotReadableException ex) {
        RestError error = new RestError();
        error.setCode(HttpStatus.BAD_REQUEST.toString());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}
