package com.am.loanwiper.controller;

import com.am.loanwiper.domain.Currency;
import com.am.loanwiper.domain.IPaymentPlan;
import com.am.loanwiper.domain.LoanType;
import com.am.loanwiper.domain.OverpayPlan;
import com.am.loanwiper.model.CalculationParameters;
import com.am.loanwiper.model.ExchangeRate;
import com.am.loanwiper.service.IRatesService;
import com.am.loanwiper.service.ISimulationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.beans.PropertyEditorSupport;

@RestController
@CrossOrigin
@Validated
public class Controller {

    private static final String CALCULATE_PLAN_PATH = "/plan";
    private static final String RATES_PATH = "/rates";
    private static final String CURRENCIES_PATH = "/currencies";

    @Autowired
    private IRatesService ratesService;

    @Autowired
    private ISimulationService simulationService;

    @RequestMapping(value = CALCULATE_PLAN_PATH, method = {RequestMethod.GET, RequestMethod.POST})
    public IPaymentPlan plan(@Valid CalculationParameters params, BindingResult bindingResult, @RequestParam(value = "type", defaultValue = "EMI") LoanType loanType, @RequestBody(required = false) OverpayPlan overpay) {
        return simulationService.simulate(params, loanType, overpay);
    }

    @RequestMapping(value = RATES_PATH, method = {RequestMethod.GET})
    public ExchangeRate rates(@RequestParam(value = "from") Currency from, @RequestParam(value = "to") Currency to) {
        return new ExchangeRate(ratesService.getExchangeRate(from, to));
    }

    @RequestMapping(value = CURRENCIES_PATH, method = {RequestMethod.GET})
    public Currency[] rates() {
        return Currency.values();
    }

    @InitBinder
    public void initBinder(final WebDataBinder webdataBinder) {
        webdataBinder.registerCustomEditor(Currency.class, new CurrencyConverter());
        webdataBinder.registerCustomEditor(LoanType.class, new LoanTypeConverter());
    }

    private class CurrencyConverter extends PropertyEditorSupport {
        public void setAsText(final String text) throws IllegalArgumentException {
            setValue(Currency.fromValue(text));
        }
    }

    private class LoanTypeConverter extends PropertyEditorSupport {
        public void setAsText(final String text) throws IllegalArgumentException {
            setValue(LoanType.fromValue(text));
        }
    }
}
