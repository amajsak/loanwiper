package com.am.loanwiper.domain;

import java.util.Arrays;

public enum LoanType {

    EMI("emi"),
    VMI("vmi");

    private String value;

    LoanType(String value) {
        this.value = value;
    }

    public static LoanType fromValue(String value) {
        for (LoanType type : values()) {
            if (type.value.equalsIgnoreCase(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException(
                "Unknown loan type " + value + ", Allowed values are " + Arrays.toString(values()));
    }

    @Override
    public String toString() {
        return value;
    }
}
