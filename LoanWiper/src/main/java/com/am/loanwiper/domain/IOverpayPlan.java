package com.am.loanwiper.domain;

import java.math.BigDecimal;

public interface IOverpayPlan {

    void addExtraPayment(int month, BigDecimal amount);

    BigDecimal getOverpay(int month);

}
