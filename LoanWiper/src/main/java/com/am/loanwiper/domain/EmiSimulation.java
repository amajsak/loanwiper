package com.am.loanwiper.domain;

import static com.am.loanwiper.domain.Constants.*;

public class EmiSimulation implements IPaymentSimulation {

    @Override
    public IPaymentPlan simulate(Amount capital, Amount interest, int monthsLeft) {
        return simulate(capital, interest, monthsLeft, null);
    }

    @Override
    public IPaymentPlan simulate(Amount capital, Amount interest, int monthsLeft, IOverpayPlan overpayPlan) {
        IPaymentPlan paymentPlan = new BasicPaymentPlan();
        Amount totalPayment = ZERO;
        Amount totalInterest = ZERO;

        Amount q = ONE.add(interest.divide(MONTHS_IN_YEAR));
        Amount qToN = q.pow(monthsLeft);
        Amount monthlyPayment = capital.multiply(qToN).multiply((q.subtract(ONE)).divide(qToN.subtract(ONE)));

        for (int i = 1; i <= monthsLeft; i++) {
            Amount monthlyInterestValue = capital.multiply(interest).multiply(MONTHS_TO_YEAR_RATIO);
            Amount monthlyCapitalPayment = monthlyPayment.subtract(monthlyInterestValue);
            totalPayment = totalPayment.add(monthlyPayment);
            totalInterest = totalInterest.add(monthlyInterestValue);

            capital = capital.subtract(monthlyCapitalPayment);
            MonthlyInstallment monthlyInstallment = new MonthlyInstallment(monthlyPayment.getValue(), monthlyInterestValue.getValue(), monthlyCapitalPayment.getValue());

            if (shouldOverpay(overpayPlan, i)) {
                Amount overpay = new Amount(overpayPlan.getOverpay(i));
                Amount afterOverpayment = capital.subtract(overpay);
                if (afterOverpayment.compareTo(ZERO) < 0) {
                    overpay = overpay.add(afterOverpayment);
                }
                monthlyCapitalPayment = monthlyCapitalPayment.add(overpay);
                monthlyPayment = monthlyPayment.add(overpay);
                capital = capital.subtract(overpay);
                totalPayment = totalPayment.add(overpay);
                monthlyInstallment.setMonthlyCapitalPayment(monthlyCapitalPayment.getValue());
                monthlyInstallment.setMonthlyPayment(monthlyPayment.getValue());
                qToN = q.pow(monthsLeft - i);
                monthlyPayment = capital.multiply(qToN).multiply((q.subtract(ONE)).divide(qToN.subtract(ONE)));
            }

            paymentPlan.addPayment(monthlyInstallment);
            if (monthlyPayment.compareTo(ZERO) <= 0) {
                break;
            }
        }
        return paymentPlan;
    }

    private boolean shouldOverpay(IOverpayPlan overpayPlan, int month) {
        return overpayPlan != null && overpayPlan.getOverpay(month) != null;
    }
}
