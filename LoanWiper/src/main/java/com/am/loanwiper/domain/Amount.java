package com.am.loanwiper.domain;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Amount {

    static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_EVEN;
    static final int SCALE = 8;
    static final MathContext ctx = new MathContext(SCALE, ROUNDING_MODE);

    private BigDecimal value;

    public BigDecimal getValue() {
        return value;
    }

    public Amount(BigDecimal value) {
        this.value = new BigDecimal(value.doubleValue(), ctx);
    }

    public Amount(String value) {
        this.value = new BigDecimal(value, ctx);
    }

    public Amount(int value) {
        this.value = new BigDecimal(value, ctx);
    }

    public Amount(double value) {
        this.value = new BigDecimal(Double.toString(value), ctx);
    }

    public Amount add(Amount a) {
        return new Amount(this.value.add(a.getValue(), ctx));
    }

    public Amount subtract(Amount a) {
        return new Amount(this.value.subtract(a.getValue(), ctx));
    }

    public Amount multiply(Amount a) {
        return new Amount(this.value.multiply(a.getValue(), ctx));
    }

    public Amount divide(Amount a) {
        return new Amount(this.value.divide(a.getValue(), SCALE, ROUNDING_MODE));
    }

    public Amount pow(int a) {
        return new Amount(this.value.pow(a, ctx));
    }

    public int compareTo(Amount a) {
        return this.value.compareTo(a.value);
    }
}
