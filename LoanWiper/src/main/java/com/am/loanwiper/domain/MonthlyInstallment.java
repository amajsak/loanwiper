package com.am.loanwiper.domain;

import java.math.BigDecimal;

public class MonthlyInstallment {

    private int monthNumber;
    private BigDecimal monthlyPayment;
    private BigDecimal monthlyInterestPayment;
    private BigDecimal monthlyCapitalPayment;

    public MonthlyInstallment(BigDecimal monthlyPayment, BigDecimal monthlyInterestPayment, BigDecimal monthlyCapitalPayment) {
        this.monthlyPayment = monthlyPayment;
        this.monthlyInterestPayment = monthlyInterestPayment;
        this.monthlyCapitalPayment = monthlyCapitalPayment;
    }

    public int getMonthNumber() {
        return monthNumber;
    }

    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public BigDecimal getMonthlyPayment() {
        return monthlyPayment.setScale(Amount.SCALE, Amount.ROUNDING_MODE);
    }

    public void setMonthlyPayment(BigDecimal monthlyPayment) {
        this.monthlyPayment = monthlyPayment;
    }

    public BigDecimal getMonthlyInterestPayment() {
        return monthlyInterestPayment.setScale(Amount.SCALE, Amount.ROUNDING_MODE);
    }

    public void setMonthlyInterestPayment(BigDecimal monthlyInterestPayment) {
        this.monthlyInterestPayment = monthlyInterestPayment;
    }

    public BigDecimal getMonthlyCapitalPayment() {
        return monthlyCapitalPayment.setScale(Amount.SCALE, Amount.ROUNDING_MODE);
    }

    public void setMonthlyCapitalPayment(BigDecimal monthlyCapitalPayment) {
        this.monthlyCapitalPayment = monthlyCapitalPayment;
    }
}
