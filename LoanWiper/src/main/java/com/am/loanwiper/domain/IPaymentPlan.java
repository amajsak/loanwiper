package com.am.loanwiper.domain;

import java.math.BigDecimal;
import java.util.Map;

public interface IPaymentPlan {

    void addPayment(MonthlyInstallment monthlyInstallment);

    BigDecimal getTotalInterestPaid();

    BigDecimal getTotalPaid();

    Map<Integer, MonthlyInstallment> getPaymentSchedule();

    void printPlan();
}
