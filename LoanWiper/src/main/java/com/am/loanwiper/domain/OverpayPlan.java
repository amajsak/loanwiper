package com.am.loanwiper.domain;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class OverpayPlan implements IOverpayPlan {

    Map<Integer, BigDecimal> overpayPlan = new HashMap<>();

    public Map<Integer, BigDecimal> getOverpayPlan() {
        return overpayPlan;
    }

    public void setOverpayPlan(Map<Integer, BigDecimal> overpayPlan) {
        this.overpayPlan = overpayPlan;
    }

    @Override
    public void addExtraPayment(int month, BigDecimal amount) {
        overpayPlan.put(month, amount);
    }

    @Override
    public BigDecimal getOverpay(int month) {
        return overpayPlan.get(month);
    }
}
