package com.am.loanwiper.domain;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class BasicPaymentPlan implements IPaymentPlan {

    private int paymentNumber = 1;
    private Map<Integer, MonthlyInstallment> paymentSchedule = new HashMap<>();

    @Override
    public void addPayment(MonthlyInstallment monthlyInstallment) {
        monthlyInstallment.setMonthNumber(paymentNumber);
        paymentSchedule.put(paymentNumber, monthlyInstallment);
        paymentNumber++;
    }

    @Override
    public BigDecimal getTotalInterestPaid() {
        BigDecimal totalInterestPaid = BigDecimal.ZERO;
        for (MonthlyInstallment monthlyInstallment : paymentSchedule.values()) {
            totalInterestPaid = totalInterestPaid.add(monthlyInstallment.getMonthlyInterestPayment());
        }
        return totalInterestPaid;
    }

    @Override
    public BigDecimal getTotalPaid() {
        BigDecimal totalPaid = BigDecimal.ZERO;
        for (MonthlyInstallment monthlyInstallment : paymentSchedule.values()) {
            totalPaid = totalPaid.add(monthlyInstallment.getMonthlyPayment());
        }
        return totalPaid;
    }

    @Override
    public Map<Integer, MonthlyInstallment> getPaymentSchedule() {
        return paymentSchedule;
    }

    @Override
    public void printPlan() {
        System.out.println("Month | Month Payment | Month Capital Payment | Month Interest Payment");
        paymentSchedule.forEach((m, i) -> System.out.println(m + ": " + i.getMonthlyPayment().setScale(Amount.SCALE, Amount.ROUNDING_MODE) + ", " +
                i.getMonthlyCapitalPayment().setScale(Amount.SCALE, Amount.ROUNDING_MODE) + ", " +
                i.getMonthlyInterestPayment().setScale(Amount.SCALE, Amount.ROUNDING_MODE)));
        System.out.println("Total paid: " + getTotalPaid().setScale(Amount.SCALE, Amount.ROUNDING_MODE));
        System.out.println("Total interest paid: " + getTotalInterestPaid().setScale(Amount.SCALE, Amount.ROUNDING_MODE));
    }
}
