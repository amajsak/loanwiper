package com.am.loanwiper.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Arrays;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Currency {

    PLN("pln", "PLN"),
    EUR("eur", "€"),
    USD("usd", "$"),
    CHF("chf", "CHF"),
    GBP("gbp", "£");

    private String key;
    private String symbol;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    Currency(String key, String symbol) {
        this.key = key;
        this.symbol = symbol;
    }

    public static Currency fromValue(String value) {
        for (Currency currency : values()) {
            if (currency.key.equalsIgnoreCase(value)) {
                return currency;
            }
        }
        throw new IllegalArgumentException(
                "Unknown currency " + value + ", Allowed values are " + Arrays.toString(values()));
    }
}
