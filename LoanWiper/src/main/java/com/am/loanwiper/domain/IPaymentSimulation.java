package com.am.loanwiper.domain;

public interface IPaymentSimulation {

    IPaymentPlan simulate(Amount capital, Amount interest, int monthsLeft);

    IPaymentPlan simulate(Amount capital, Amount interest, int monthsLeft, IOverpayPlan overpayPlan);

}