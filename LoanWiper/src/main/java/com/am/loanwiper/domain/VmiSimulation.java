package com.am.loanwiper.domain;

import static com.am.loanwiper.domain.Constants.ONE;
import static com.am.loanwiper.domain.Constants.ZERO;

public class VmiSimulation implements IPaymentSimulation {

    @Override
    public IPaymentPlan simulate(Amount capital, Amount interest, int monthsLeft) {
        return simulate(capital, interest, monthsLeft, null);
    }

    @Override
    public IPaymentPlan simulate(Amount capital, Amount interest, int monthsLeft, IOverpayPlan overpayPlan) {
        IPaymentPlan paymentPlan = new BasicPaymentPlan();
        Amount totalPayment = ZERO;
        Amount totalInterest = ZERO;

        Amount startingCapital = new Amount(capital.getValue());

        for (int i = 1; i <= monthsLeft; i++) {

            Amount leftToPay = startingCapital.add(totalInterest).subtract(totalPayment);
            Amount monthlyCapitalPayment = leftToPay.divide(new Amount(monthsLeft + 1 - i));
            Amount monthlyPayment = monthlyCapitalPayment
                    .multiply(ONE.add((new Amount(monthsLeft - i + 1)).multiply(interest.divide(Constants.MONTHS_IN_YEAR))));

            Amount monthlyInterestPayment = monthlyPayment.subtract(monthlyCapitalPayment);

            if (shouldOverpay(overpayPlan, i)) {
                Amount overpay = new Amount(overpayPlan.getOverpay(i));

                Amount capitalToPay = startingCapital.subtract(totalPayment.subtract(totalInterest).subtract(monthlyInterestPayment).add(monthlyPayment));
                Amount afterOverpayment = capitalToPay.subtract(overpay);
                if (afterOverpayment.compareTo(ZERO) < 0) {
                    overpay = overpay.add(afterOverpayment);
                }

                monthlyCapitalPayment = monthlyCapitalPayment.add(overpay);
                monthlyPayment = monthlyPayment.add(overpay);
                capital = capital.subtract(overpay);
            }

            totalPayment = totalPayment.add(monthlyPayment);
            totalInterest = totalInterest.add(monthlyInterestPayment);
            paymentPlan.addPayment(new MonthlyInstallment(monthlyPayment.getValue(), monthlyInterestPayment.getValue(), monthlyCapitalPayment.getValue()));

            if (startingCapital.subtract(totalPayment.subtract(totalInterest)).compareTo(ZERO) <= 0) {
                break;
            }
        }
        return paymentPlan;
    }

    private boolean shouldOverpay(IOverpayPlan overpayPlan, int month) {
        return overpayPlan != null && overpayPlan.getOverpay(month) != null;
    }
}
