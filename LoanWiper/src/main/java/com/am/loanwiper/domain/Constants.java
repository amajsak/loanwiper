package com.am.loanwiper.domain;

public final class Constants {

    static final Amount ZERO = new Amount("0");
    static final Amount ONE = new Amount("1");
    final static Amount MONTHS_IN_YEAR = new Amount("12");
    final static Amount MONTHS_TO_YEAR_RATIO = new Amount(1.0 / 12.0);

    private Constants() {
    }
}
