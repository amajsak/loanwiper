package com.am.loanwiper.model;

import javax.validation.constraints.*;

public class CalculationParameters {

    @NotNull
    @Min(1)
    @DecimalMax("10000000")
    double capital;
    @NotNull
    @Min(1)
    @Max(600)
    private int months;
    @NotNull
    @DecimalMin("0.0001")
    @Max(1)
    private double interest;

    public double getCapital() {
        return capital;
    }

    public void setCapital(double capital) {
        this.capital = capital;
    }

    public int getMonths() {
        return months;
    }

    public void setMonths(int months) {
        this.months = months;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }
}
