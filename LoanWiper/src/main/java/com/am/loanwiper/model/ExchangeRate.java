package com.am.loanwiper.model;

public class ExchangeRate {

    double rate;

    public ExchangeRate(double rate) {
        this.rate = rate;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
