package com.am.loanwiper.service;

import com.am.loanwiper.domain.Currency;
import org.springframework.web.client.RestTemplate;

public interface IRatesService {

    double getExchangeRate(Currency from, Currency to);

    void setRestTemplate(RestTemplate restTemplate);

}
