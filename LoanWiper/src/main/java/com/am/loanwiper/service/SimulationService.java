package com.am.loanwiper.service;

import com.am.loanwiper.domain.*;
import com.am.loanwiper.model.CalculationParameters;
import org.springframework.stereotype.Service;

@Service
public class SimulationService implements ISimulationService {

    @Override
    public IPaymentPlan simulate(CalculationParameters parameters, LoanType loanType, IOverpayPlan overpayPlan) {
        Amount capital = new Amount(parameters.getCapital());
        Amount interest = new Amount(parameters.getInterest());
        int monthsLeft = parameters.getMonths();

        IPaymentSimulation simulation;
        switch (loanType) {
            case VMI:
                simulation = new VmiSimulation();
                break;
            default:
                simulation = new EmiSimulation();
        }
        return simulation.simulate(capital, interest, monthsLeft, overpayPlan);
    }
}
