package com.am.loanwiper.service;

import com.am.loanwiper.domain.IOverpayPlan;
import com.am.loanwiper.domain.IPaymentPlan;
import com.am.loanwiper.domain.LoanType;
import com.am.loanwiper.model.CalculationParameters;

public interface ISimulationService {

    IPaymentPlan simulate(CalculationParameters parameters, LoanType loanType, IOverpayPlan overpayPlan);

}
