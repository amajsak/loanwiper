package com.am.loanwiper.service;

import com.am.loanwiper.domain.Currency;
import com.am.loanwiper.model.RatesTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class RatesService implements IRatesService {

    private final static String RATES_API_URL = "http://api.nbp.pl/api/exchangerates/rates/a/{currency}";
    private final static String CURRENCY_KEY = "currency";

    private final Logger logger = LoggerFactory.getLogger(RatesService.class);
    private RestTemplate restTemplate = new RestTemplate();

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    @Cacheable("rate")
    public double getExchangeRate(Currency from, Currency to) {
        double fromRate = from.equals(Currency.PLN) ? 1 : Double.parseDouble(getRates(from).getRates().get(0).getMid());
        double toRate = to.equals(Currency.PLN) ? 1 : Double.parseDouble(getRates(to).getRates().get(0).getMid());
        return fromRate / toRate;
    }

    private RatesTable getRates(Currency currencyCode) {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put(CURRENCY_KEY, currencyCode.name());
        RatesTable rate = restTemplate.getForObject(RATES_API_URL, RatesTable.class, urlVariables);
        logger.info("Retrieved exchange rate from API: " + rate);
        return rate;
    }


}
