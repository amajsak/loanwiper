angular.
    module('core.calculation').
    factory('CalculationService', ['$resource',
        function($resource) {
            return $resource('http://localhost:8080/plan', {}, {
                withOverpay: {
                    method: 'POST'
                },
                defaultPlan: {
                    method: 'GET'
                }
            });
        }
    ]);