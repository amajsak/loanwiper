angular.
    module('core.rates').
    factory('RatesService', ['$resource',
        function($resource) {
            return {
                Rates: $resource('http://localhost:8080/rates', {}, {
                    query: {
                        method: 'GET',
                        isArray: false
                    }
                }),
                Currencies : $resource('http://localhost:8080/currencies', {}, {
                    query: {
                        method: 'GET',
                        isArray: true
                    }
                })
            }}
        ]);