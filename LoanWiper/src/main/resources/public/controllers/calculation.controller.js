angular.
    module('core').
    controller('Calculation', ['$scope', 'CalculationService', 'RatesService', function($scope, CalculationService, RatesService) {
        $scope.currencies = RatesService.Currencies.query();
        $scope.loanCurrency = {key: 'pln', symbol: 'PLN'};
        $scope.query = {type: 'emi'};
        $scope.overpayments = [];
        $scope.defaultCalc = {};
        $scope.overpaidCalc = {};
        $scope.angular = angular;

        $scope.submit = function() {
            if ($scope.query.capital && $scope.query.interest && $scope.query.months) {
                if ($scope.overpayments.length > 0 && $scope.overpayments[0]["month"] !== undefined) {
                    var fullCalc = CalculationService.withOverpay({capital: $scope.query.capital, interest: $scope.query.interest, months: $scope.query.months, type: $scope.query.type}, {overpayPlan:buildOverpayPlan($scope.overpayments)});
                    $scope.overpaidCalc = fullCalc;
                } else {
                    $scope.overpaidCalc = {};
                }
                var defaultCalc = CalculationService.defaultPlan({capital: $scope.query.capital, interest: $scope.query.interest, months: $scope.query.months, type: $scope.query.type});
                $scope.defaultCalc = defaultCalc;
            }
        };

        $scope.exchange = function(exchangeCurrency) {
            var exRate = RatesService.Rates.query({from: $scope.loanCurrency.key, to: exchangeCurrency.key});
            exRate.$promise.then(function(d) {
                $scope.exchangeCurrency = exchangeCurrency;
                $scope.exchangedPaid = $scope.defaultCalc.totalPaid * d.rate;
                $scope.exchangedInterest = $scope.defaultCalc.totalInterestPaid * d.rate;
                if (!angular.equals({}, $scope.overpaidCalc)) {
                    $scope.exchangedOpPaid = $scope.overpaidCalc.totalPaid * d.rate;
                    $scope.exchangedOpInterest = $scope.overpaidCalc.totalInterestPaid * d.rate;
                }
            });
        }

        $scope.addOverpayment = function() {
            var index = $scope.overpayments.length + 1;
            $scope.overpayments.push({});
        };

        $scope.removeOverpayment = function() {
            var index = $scope.overpayments.length - 1;
            $scope.overpayments.splice(index);
        };

        $scope.defaultTableClass = "col-lg-5 col-sm-5 col-md-offset-3";
        $scope.defaultSummaryClass = "col-lg-3 col-sm-3 col-md-offset-3";

        $scope.$watch('overpaidCalc', function(overpaidCalc) {
             if (!angular.equals({}, $scope.overpaidCalc)) {
                 $scope.defaultTableClass = "col-lg-5 col-sm-5 col-md-offset-1";
                 $scope.defaultSummaryClass = "col-lg-3 col-sm-3 col-md-offset-1";
             } else {
                 $scope.defaultTableClass = "col-lg-5 col-sm-5 col-md-offset-3";
                 $scope.defaultSummaryClass = "col-lg-3 col-sm-3 col-md-offset-3";
             }
        }, true);
    }]);

function buildOverpayPlan (overpayments) {
    var plan = {};
    for (i in overpayments) {
        var month = overpayments[i]['month'];
        var amount = overpayments[i]['amount'];
        if (month !== undefined && amount !== undefined) {
            if (plan[month] !== undefined) {
                plan[month] += amount;
            } else {
                plan[month] = amount;
            }
        }
    }
    return plan;
}