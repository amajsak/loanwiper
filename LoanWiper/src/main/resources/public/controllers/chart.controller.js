angular.
    module('core').
    controller("ChartController", function ($scope) {
        $scope.data= [[], []];
        $scope.series = ['Default Payments'];
        if ($scope.$parent.overpaidCalc) {
            $scope.series.push('With Overpayments');
        }

        $scope.$watch('$parent.defaultCalc', function (defaultCalc) {
            getLabels(defaultCalc);
        }, true);

        $scope.$watch('$parent.overpaidCalc', function (overpaidCalc) {
            getLabels(overpaidCalc);
        }, true);

        $scope.$watch('$parent.defaultCalc', function (defaultCalc) {
            populateData(defaultCalc, 0);
        }, true);

        $scope.$watch('$parent.overpaidCalc', function (overpaidCalc) {
            populateData(overpaidCalc, 1);
        }, true);

        function getLabels (calc) {
            if (calc.$resolved) {
                $scope.labels = Object.keys(calc.paymentSchedule).map(function(key) {
                    return key;
                });
            }
        };

        function populateData (calc, index) {
            if (calc.$resolved) {
                var capitalToPay = calc.totalPaid - calc.totalInterestPaid
                var monthlyPayments = Object.keys(calc.paymentSchedule).map(function(key) {
                    return calc.paymentSchedule[key]["monthlyCapitalPayment"];
                });
                var leftToPay = [];
                leftToPay.push(round(capitalToPay - monthlyPayments[0]));
                for (var i = 1; i < monthlyPayments.length; i++) {
                    leftToPay.push(round(leftToPay[i-1] - monthlyPayments[i]));
                }
                $scope.data[index] = leftToPay;
            }
        }
   });

function round (number) {
    return Math.round(number * 100) / 100;
}