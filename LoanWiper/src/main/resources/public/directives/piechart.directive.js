angular.
    module('core').
    directive('piechart', function () {
        return {
            restrict: 'A',
            templateUrl: 'templates/piechart.html',
            scope: {
                plan: '='
            },
            controller: function ($scope) {
                $scope.labels = ["Interest", "Capital"];
                $scope.$watch('plan', function(plan) {
                   if (plan.$resolved) {
                       $scope.data = [];
                       $scope.data.push(round(plan.totalInterestPaid));
                       $scope.data.push(round(plan.totalPaid - plan.totalInterestPaid));
                   }
               }, true);
            }
        }
    });