angular.
    module('core').
    directive('paymentsTable', function () {
        return {
            restrict: 'A',
            templateUrl: 'templates/payments-table.html',
            scope: {
                plan: '='
            },
            controller: function ($scope) {
                $scope.angular = angular;
            }
        }
    });